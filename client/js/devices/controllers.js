'use strict';

/* Device Controller */

angular.module('piAutomation.devices.controllers', []).
  controller('DevicesController', ['$scope', 'DeviceService', function($scope, DeviceService) {
		var apiResponse = DeviceService.query(function () {
            if (apiResponse.code === 1){
                $scope.devices = apiResponse.content;
                console.log($scope.devices);
                $scope.successful = true;
                $scope.message = "Successfully retrieved devices";
            }else{
                $scope.failure = true;
                $scope.devices = [];
                $scope.message = apiResponse.content;
            }
        });
		
		$scope.device = {name: "", driverLocation: "", deviceLocation: "", type: ""};

        $scope.successful = false;
        $scope.failure = false;

        $scope.action = "";
        $scope.title = "";


		$scope.save = function (){
            if ($scope.action == "Edit"){


            }else{
                DeviceService.save($scope.device, function (err) {
                    if (err){
                        console.log(err)
                        $scope.failure = true;
                        $scope.message = "Error adding device";
                    }else{
                        $scope.successful = true;
                        $scope.message = "Successfully added the device to the list"
                    }

                    $scope.news = DeviceService.query();
                });
            }
		};

        $scope.create = function () {
            $scope.device = {name: "", driverLocation: "", deviceLocation: "", type: ""};
            $scope.action = "Add";
            $scope.title = "Create";
        }

		$scope.edit = function () {
            $scope.device = {name: "", driverLocation: "", deviceLocation: "", type: ""};
            $scope.action = "Edit";
            $scope.title = "Save";
		};
  }]);