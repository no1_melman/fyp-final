/**
 * Created by Callum on 07/04/14.
 */

angular.module('piAutomation.events.controllers', []).
    controller('EventsController', ['$scope', 'EventService', function($scope, EventService) {
        $scope.successful = false;
        $scope.failure = false;

        var apiResponse = EventService.query(function (){

            if (apiResponse.code === 1){
                for (var i = 0; i < apiResponse.content.length; i++){
                    var event = apiResponse.content[i];
                    event.eventType = replaceEventType(event.eventType);
                    event.when = replaceTime(event.when);
                }
                $scope.events = apiResponse.content;
                console.log($scope.events);
            }else{
                $scope.failure = true;
                $scope.message = apiResponse.content;
            }

        });

    }]);


function replaceEventType(number){
    if (number == 1)
        return "Immediate";
    if (number == 2)
        return "Reading";
    if (number == 3)
        return "Scheduled";
    if (number == 4)
        return "Timer";

    return "Unknown";
}

function replaceTime(ms){
    return new Date(ms);
}