'use strict';

// Declare app level module which depends on filters, and services
angular.module('piAutomation', [
    'ngRoute',
    'piAutomation.filters',
    'piAutomation.services',
    'piAutomation.directives',
    'piAutomation.controllers',
    'piAutomation.devices.controllers',
    'piAutomation.events.controllers'
]).
config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'partials/home.html', controller: 'HomeController'});

    $routeProvider.when('/devices', {templateUrl: 'partials/devices/list.html', controller: 'DevicesController'});

    $routeProvider.when('/events', {templateUrl: 'partials/events/list.html', controller: 'EventsController'});

    $routeProvider.otherwise({redirectTo: '/'});
}]);


/*

 $routeProvider.when('/news', {templateUrl: 'partials/news.html', controller: 'NewsController'});

 $routeProvider.when('/devices/create', {templateUrl: 'partials/deviceAction/edit.html', controller: 'DeviceActionCreateController'});
 $routeProvider.when('/devices/edit/:id', {templateUrl: 'partials/deviceAction/edit.html', controller: 'DeviceActionEditController'});
 $routeProvider.when('/devices/delete/:id', { controller: 'DeviceActionDeleteController' } );

 */