'use strict';

/* News Controller */

angular.module('piAutomation.news.controllers', []).
  controller('NewsController', ['$scope', 'NewsService', function($scope, NewsService) {
		$scope.news = NewsService.query();
  }]);