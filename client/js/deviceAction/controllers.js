/**
 * Created by Callum on 16/02/14.
 */

angular.module('piAutomation.deviceAction.controllers', []).
    controller('DeviceActionController', ['$scope', 'DeviceActionService', '$location', 'PfioService',function($scope, DeviceActionService, $location, PfioService) {
        $scope.deviceActions = DeviceActionService.query();

        $scope.successful = false;
        $scope.failure = false;
        $scope.message = "";

	$scope.fire = function () {
		PfioService.query();
	}

        $scope.create = function () {
            $location.path('/deviceActions/create');
        }
    }])
    .controller('DeviceActionCreateController', ['$scope', 'DeviceActionService', '$location', function ($scope, DeviceActionService, $location){
        $scope.deviceAction = {pin:-1, action: 1, affectedDeviceIds:new Array()};

        $scope.actions = [{value: 1, text: "Fire"},
            {value: 2, text: "Schedule"},
            {value: 3, text: "Timer"},
            {value: 4, text: "Event"}];

        $scope.save = function () {
            DeviceActionService.save($scope.deviceAction, function (err){
                if (err){
                    console.log(err);
                }
                $location.path('/deviceActions');
            });
        };
    }])
    .controller('DeviceActionEditController', ['$scope', 'DeviceActionService', '$location', '$routeParams', function($scope, DeviceActionService, $location, $routeParams){
        $scope.actions = [{value: 1, text: "Fire"},
            {value: 2, text: "Schedule"},
            {value: 3, text: "Timer"},
            {value: 4, text: "Event"}];

        var id = $routeParams.id

        DeviceActionService.get({id: id}, function(resp) {
            $scope.deviceAction = resp.content;

        })

        $scope.action = "Update"

        $scope.save = function() {
            DeviceActionService.update({id: id}, $scope.car, function() {
                $location.path('/deviceActions')
            })
        }
    }])
    .controller('DeviceActionDeleteController', ['$scope', 'DeviceActionService', '$location', '$routeParams', function($scope, DeviceActionService, $location, $routeParams){
        var id = $routeParams.id

        DeviceActionService.del({id: id}, function() {
            $location.path('/deviceActions')
        });
    }]);