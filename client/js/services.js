'use strict';

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('piAutomation.services', ['ngResource'])
	.factory('NewsService', ['$resource', function($resource) {
		return $resource('/api/news/:id', {id: '@id'}, {update: {method: 'PUT'}});
	}])
	.factory('DeviceService', ['$resource', function($resource) {
		return $resource('/api/devices/:id', {id: '@id'}, {
            update: {method: 'PUT', isArray: false},
            query: {method: 'GET', isArray: false}
        });
	}])
    .factory('DeviceActionService', ['$resource', function ($resource) {
        return $resource('/api/deviceAction/:id', {id: '@id'}, {update:{method:'PUT'}});
    }])
	.factory('PfioService', ['$resource', function ($resource) {
        return $resource('/api/pfio/:id', {id: '@id'}, {update:{method:'PUT', isArray: false}});
    }])
    .factory('EventService', ['$resource', function ($resource) {
        return $resource('/api/events/:id', {id: '@id'}, {
            update: { method:'PUT', isArray: false },
            query: { method: 'GET', isArray: false }
        });
    }]);
