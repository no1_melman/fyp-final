if (!process.env.NODE_ENV) process.env.NODE_ENV = 'development'

//========================= Node Scripts =========================//
var db = require('mongoose'), // get the db
    express = require('express'),
    http = require('http'),
    path = require('path'),
    async = require('async'),
    colors = require('colors'),
    util = require('util'),
    clientDir = path.join(__dirname, 'client'),
    pfio = require('piface-node');

//========================= Custom Scripts =========================//
var pfioServer = require('./server/modules/pfio/pfio-server'),
    Binary = require('./server/helpers/binaryHelper'),
    pfioState = require('./server/modules/pfio/pfioState'),
    EventBus = require('./server/modules/EventBus'),
    eventQueue = require('./server/modules/events/eventQueue'),
    eventLoop = require('./server/modules/events/eventLoop'),
    eventHandler = require('./server/modules/events/eventProcess');

var app = express();

app.configure(function () {
    app.set('port', process.env.PORT || 3000);
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(app.router);
    app.use(express.static(clientDir));
});

app.configure('development', function () {
    app.use(express.errorHandler());
})

// Once the PiFace is listening start up the routes
EventBus.once('pfio.server.start', function () {
    console.log("%s", colors.green("Successfully started PiFace listening"));
    // set up the routes
    require('./server/config/expressRoutes')(app, clientDir, path);
    require('./server/config/eventRoutes');

    async.series([function (done){
        // compile the events queue
        eventLoop.readEvents(done);
    }, function (done) {
        // start reading the event queue
        eventLoop.executeEvent();
        done();
    }], function (err){
        if (err){
            console.log(colors.red(util.inspect(err)));
            return;
        }

        console.log(colors.green("Events read, and being processed!"));
    });
});

function StartPfioServer() {
    // start the PiFace listening           
    pfioServer.start();
}

function SetupWebServer() {
    var server = http.createServer(app);

    server.listen(app.get('port'), function () {
        console.log("Web server listening in %s on port %d", colors.green(process.env.NODE_ENV), app.get('port'));
        StartPfioServer(); // Start the PiFace server now
    });
}

// connect to the db
db.connect('mongodb://localhost/piautomation');

var dbConn = db.connection;
dbConn.on('error', console.error.bind(console, 'connection error:'));
dbConn.once('open', function () {
    console.log("Successfully %s to the database", colors.green("connected"));
    async.series([
        function (dbDone) {
            // check to make sure all the piface devices are in the database
            require('./server/config/deviceScan')(dbDone);
        },
        function (dbDone) {
            // Seed the database with some events to do
            require('./server/config/setupEvents')(dbDone);
        }],
    function (err, result) {
        if (err) {
            console.log("%s", colors.red(util.inspect(err)));
            return;
        }

        console.log(colors.green(result));
        SetupWebServer(); // Load the web server
    });
});

var gracefulShutdown = function () {
    pfioServer.stop(pfioState.allOff());
    dbConn.close();
    process.exit(0);
}

process.on('SIGINT', gracefulShutdown);
