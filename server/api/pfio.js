/* Created by Callum on 05/02/14. */
var Binary = require('../helpers/binaryHelper'),
    EventBus = require('../modules/EventBus'),
    eventQueue = require('../modules/events/eventQueue'),
    pfioState = require('../modules/pfio/pfioState'),
    events = require('../config/dbModels').event,
    devices = require('../config/dbModels').device,
    colors = require('colors'),
    util  = require('util'),
    async = require('async');

function getDbEntity (dbEntity, expression, callback) {
    var query = dbEntity.findOne(expression);

    query.exec(function (err, entity) {
        if (err) {
            callback(err);
            return;
        }

        if (entity === null) {
            callback("Entity Empty");
            return;
        }

        callback(null, entity);
    });

    return;
}

function formatRespData (code, content) {
    if (typeof code === 'object') {
        content = code;
        code = 1; //0 failure, 1 = success
    }

    return {
        code: code,
        content: content
    }
}

function modifyPfioState(event, fromEventBus, internalEventId) {
    if (typeof event === "boolean")
        return; // check to make sure that there wasn't error
    if (typeof event !== "object")
        return; // check to make sure that obj is actually an object

    // determine whether there are affected outputs
    if (!(event.deviceOutputs instanceof Array))
        return; // next check that it has deviceOutputs and these Outputs are an array
    if (!(event.deviceOutputs.length > 0))
        return; // then check that there are some devices to affect

    // if we get here, the deviceOutputs is an Array and has values
    var currentState = pfioState.current.toArray();

    async.each(event.deviceOutputs, function (id, callback) { // iterate through all the device outputs
        devices.findById(id, function (err, device){ // find them in the database
            if (err){ // check there was no error, if there was then pass it back to the callback
                callback(err);
                return;
            }

            var outputState = currentState[device.value]; // get the current state of output of the device that has been found
            currentState[device.value] = outputState === 0 ? 1 : 0; // assign the reversed value back to the current state
            callback(); // its now done so fire the callback to allow the others to continue
        });
    }, function (err) {
        if (err) {
            console.log("Async: %s", colors.red(util.inspect(err))); // if there was an error, get the object as a readable error
            return;
        }
        // set the pfio current state to the state just created by the above array
        pfioState.current = new Binary(currentState); // keeps the current state of the piface

        EventBus.emit('pfio.change.output', pfioState.current.toDecimal());  // emits an event to write to the piface output

        // check if this was from the event queue
        if (!(typeof fromEventBus === 'boolean' && fromEventBus))
            return;

        // if it was from the event queue we need to find the corresponding event
        for (var i = 0; i < eventQueue.length; i++)
            if (eventQueue[i].id === internalEventId) { // find the event by its ID
                eventQueue[i].currentlyProcessingEvent = false; // once we found it, we can now say we've finished dealing with it
                if (!eventQueue[i].repeat)
                    eventQueue.splice(i, 1); // if the event is not to be repeated then splice it from the queue
            }
    });
};

function selectEvent(events, device){
    for (var i = 0; i < events.length; i++){
        var event = events[i];

        if (event.deviceInput.toHexString() === device.id)
            return event;
    }

    return null;
}

module.exports = function () {
    return {
        fireEvent: function (pin, state) {
            console.log("Event Information: Pin: %s; State: %s", colors.yellow(util.inspect(pin)), colors.yellow(util.inspect(state)))

            if (!state)
                return; // Usually means that its a button depress, don't really want to do anything with that

            devices.findOne({value: pin, input: true}, function (err, foundDevice) {
                if (err){
                    console.log("Error in %s, Message: %s", colors.red("Fire Event, PFIO Api"), colors.red(util.inspect(err)));
                    return;
                }

                // make sure we only find event types that are immediate
                events.find({eventType: { $lte: 2 }}, function (err, foundEvents){
                    if (err){
                        console.log("Event Find Error: %s", colors.red(util.inspect(err)));
                        return;
                    }

                    var selectedEvent = selectEvent(foundEvents, foundDevice);

                    if (selectedEvent === null)
                        return;

                    modifyPfioState(selectedEvent);
                });
            });
        },
        fire: function (req, res) {
            var knownOutputs = [1,3,5,7];
            var currentState = pfioState.current.toArray();

            for (var i = 0; i < knownOutputs.length; i++) {
                var outputState = currentState[knownOutputs[i]];
                currentState[knownOutputs[i]] = outputState === 0 ? 1 : 0;
            }

            pfioState.current = new Binary(currentState); // keeps the current state of the piface

            EventBus.emit('pfio.change.output', pfioState.current.toDecimal());  // writes to the piface output

            res.json(formatRespData({state: pfioState.current.toDecimal()}));
        },
        modifyPfioState: modifyPfioState
    };
};