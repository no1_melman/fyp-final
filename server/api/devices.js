var devices = require('../config/dbModels').device,
    Device = require('../config/dbModels').device;

module.exports = function() {
    return {
        list : function (req, res) {
            var offset = ~~req.query.offset || 0,
                limit = ~~req.query.limit || 25;

            devices.find({}, function (err, devicesFound) {
                if (err)
                    res.json(formatRespData(0, err))
                else
                    res.json(formatRespData(devicesFound.slice(offset*limit, offset*limit + limit)));
            });
        },
        total: function (req, res) {
            var total = 0;
            devices.find({}, function (err, devicesFound) {
                if (err)
                    res.json(formatRespData(0, err));
                else
                    res.json({total: devicesFound.length});
            });
        },
        // --------- CRUD ----------\\
        create : function(req, res) {
            var jsonDevice = req.body; // get the body of the JSON

            var device = new Device(jsonDevice); // create that new device
            device.save(function (err){ // save it in the database
                if(err)
                    res.json(formatRespData(0, err));
                else
                    res.json(formatRespData(1, "success")); // if success send out the success
            });
        },
        read: function (req, res){
            var id = req.params.id;

            devices.findById(id, function (err, device){
                if (!device)
                    res.json(formatRespData(0, "Can't find device with id: " + id));
                else
                    res.json(formatRespData(device));
            });
        },
        update: function (req, res){
            var id = req.params.id;

            var jsonEvent = req.body;

            devices.findByIdAndUpdate(id,  jsonEvent, function (err, device){
                if (!device)
                    res.json(formatRespData(0, "Can't find device action with id: " + id));
                else
                    res.json(formatRespData("Success"));
            });
        },
        del: function (req, res){
            var id = req.params.id;

            devices.findByIdAndRemove(id, function (err, device){
                if(err)
                    res.json(formatRespData(0, err));
                else if (!device)
                    res.json(formatRespData(0, "Couldn't find device"));
                else
                    res.json(formatRespData(1, "Successfully deleted device"));
            });
        }
	};
};

function formatRespData (code, content) {
  if (typeof code === 'object') {
    content = code;
    code = 1; //0 failure, 1 = success
  }

  return {
    code: code,
    content: content
  };
};