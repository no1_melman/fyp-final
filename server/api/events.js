var events = require('../config/dbModels').event,
    Event = require('../config/dbModels').event,
    devices = require('../config/dbModels').device,
    colors = require('colors'),
    util = require('util');

function compareIds(id,id2){
    return id == id2.toHexString();
}

function replaceDevices(devices, deviceIds){
    var devicesFound = [];

    for (var i = 0; i < devices.length; i++){
        var device = devices[i];

        for (var y = 0; y < deviceIds.length; y++){
            var deviceId = deviceIds[y]; // not even getting hit, need to look into what device outputs is doing here

            if (compareIds(device.id, deviceId)){
                devicesFound.push(device);
                continue;
            }
        }

        if (devicesFound.length >= deviceIds.length){
            return devicesFound;
        }
    }
}

function replaceDevice(devices, deviceId){
    if (deviceId === undefined)
        return null;

    for (var i = 0; i < devices.length; i++){
        var device = devices[i];

        if (compareIds(device.id, deviceId)){
            return device;
        }
    }
}

function createEventObj(event, devicesFound){
    var deviceOutputs = replaceDevices(devicesFound, event.deviceOutputs);
    var deviceInput = event.deviceInput !== null ? replaceDevice(devicesFound, event.deviceInput) : null;

    event.deviceOutputs = deviceOutputs;
    event.deviceInput = deviceInput;

    var eventObj = {
        id: event.id,
        when: event.when,
        eventType: event.eventType,
        repeat: event.repeat,
        value: event.value
    };

    if (deviceInput)
        eventObj.deviceInput = deviceInput;
    if (deviceOutputs)
        eventObj.deviceOutputs = deviceOutputs;

    return eventObj;
}

module.exports = function() {
    /* now db is always accessible within this scope */
    return {
        list : function (req, res) {
            events.find({}, function (err, eventsFound) {
                if (err) {
                    res.json(formatRespData(0, err));
                    return;
                }

                devices.find({}, function (err, devicesFound){
                    if (err) {
                        res.json(formatRespData(0, err));
                        return;
                    }

                    var modifiedEvents = [];

                    for (var i = 0; i < eventsFound.length; i++){
                        var event = eventsFound[i];

                        modifiedEvents.push(createEventObj(event, devicesFound));
                    }

                    res.json(formatRespData(modifiedEvents));
                });
            });
        },
        total: function (req, res) {
            var total = 0;
            events.find({}, function (err, eventsFound) {
                if (err)
                    res.json(formatRespData(0, err));
                else
                    res.json({total: eventsFound.length});
            });
        },
        // --------- CRUD ----------\\
        create : function(req, res) {
            var jsonEvent = req.body

            var event = new Event(jsonEvent);
            event.save(function (err){
                if(err)
                    res.json(formatRespData(0, err));
                else
                    res.json(formatRespData(1, "success"));
            })
        },
        read: function (req, res){
            var id = req.params.id;

            events.findById(id, function (err, event){
                if (!event)
                    res.json(formatRespData(0, "Can't find device action with id: " + id))
                else
                    res.json(formatRespData(event))
            });
        },
        update: function (req, res){
            var id = req.params.id;

            var jsonEvent = req.body;

            events.findByIdAndUpdate(id,  jsonEvent, function (err, event){
                if (!event)
                    res.json(formatRespData(0, "Can't find device action with id: " + id));
                else
                    res.json(formatRespData("Success"));
            });
        },
        del: function (req, res){
            var id = req.params.id;

            events.findByIdAndRemove(id, function (err, event){
                if(err)
                    res.json(formatRespData(0, err));
                else if (!event)
                    res.json(formatRespData(0, "Couldn't find event"));
                else
                    res.json(formatRespData(1, "Successfully deleted device action"));
            });
        }
    };
};

function formatRespData (code, content) {
    if (typeof code === 'object') {
        content = code;
        code = 1; //0 failure, 1 = success
    }

    return {
        code: code,
        content: content
    }
}