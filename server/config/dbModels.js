/**
 * Created by Callum on 21/01/14.
 */
var db = require('mongoose'); // we only need to create a connection in the
// server initial start up phase, mongoose will automatically pick the connection
// to use.

var schema = db.Schema;

/******* DEFINE SCHEMAS *******/
// News Schema
var newsSchema = new schema({
    title:  String,
    author: String,
    content:   String,
    date: { type: Date, default: Date.now }
});

// Device Schema
var deviceSchema = new schema({
    name: String,
    deviceLocation: String,
    type: String,
    dateAdded: { type: Date, default: Date.now },
    value: Number,
    input: Boolean
});

// Event Schema
// eventTypes: immediate, reading, timer, scheduled
// the interval eventType is defined by the repeat boolean.
var eventSchema = new schema({
    eventType: Number,
    deviceInput: schema.Types.ObjectId,
    deviceOutputs: [ schema.Types.ObjectId ],
    when: Number,
    repeat: Boolean,
    value: Number
});

exports.event = db.model('Event', eventSchema);
exports.news = db.model('News', newsSchema);
exports.device = db.model('Device', deviceSchema);

/*
news = db.model('News', newsSchema);
News: new news(), // these are only for adding to the database
Device: db.model('Device', deviceSchema),
Event: db.model('Event', eventSchema)
*/
