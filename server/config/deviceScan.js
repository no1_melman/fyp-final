/**
 * Created by Callum on 23/03/14.
 */
var colors = require('colors'),
    async = require('async'),
    devices = require('./dbModels').device;

module.exports = function (done) {
    var arr = [
        {type: "button 1", pin: 0},
        {type: "button 2", pin: 1},
        {type: "button 3", pin: 2},
        {type: "button 4", pin: 3},
        {type: "LED 1", pin: 0},
        {type: "LED 2", pin: 1},
        {type: "LED 3", pin: 2},
        {type: "LED 4", pin: 3},
        {type: "LED 5", pin: 4},
        {type: "LED 6", pin: 5},
        {type: "LED 7", pin: 6},
        {type: "LED 8", pin: 7}
    ]; // create an array with all the devices found on the PiFace module

    async.each(arr, function (item, callback) {
        var obj = { // create a database object from the array object
            name: item.type,
            value: item.pin, // this is used for pfio api to know what needs to do what
            input: item.type.indexOf("button") > -1 // this is to differentiate from buttons and LEDs
        };

        devices.findOne({name: item.type}, function (err, device) {
            if (err) {
                callback(err);
                return; // if there was an error send it through and return out the function
            } else if (device) {
                console.log("Found %s, already %s", colors.green(item.type), colors.red("logged!"));
                callback();
                return; // if there is already a device logged with the same name then just say so and return out the function
            }

            var newDevice = new devices(obj); // other wise create a new object with the device information

            newDevice.save(function (err, device) { // save the device into the database
                if (err) { // if there was an error send it through to the callback
                    callback(err);
                    return; // return out the function after error
                }

                console.log("Found %s", colors.green(item.type)); // log that the device saved successfully

                callback(); // send through to the async that we have been successful
            });
        });
    }, function (err) {
        if (err)
            done(err);
        else
            done();
    });
}
