var devicesApi = require('../api/devices')(), // pass the devices api the Devices model
    eventsApi = require('../api/events')(), // pass the events api the Event model
    pfioApi = require('../api/pfio')(),
    colors = require('colors'),
    util = require('util');


// Express Configuration of
module.exports = function (app, clientDir, path) {
    app.get('/', function (req, res) {
        res.sendfile(path.join(clientDir, 'index.html'))
    });

    app.get('/api/devices', devicesApi.list);
    app.get('/api/devices/:id', devicesApi.read);
    app.get('/api/devices/total', devicesApi.total); // placement matters
    app.post('/api/devices', devicesApi.create);
    app.put('/api/devices/:id', devicesApi.update);
    app.del('/api/devices/:id', devicesApi.del);

    app.get('/api/events', eventsApi.list);
    app.get('/api/events/:id', eventsApi.read);
    app.post('/api/events', eventsApi.create);
    app.put('/api/events/:id', eventsApi.update);
    app.del('/api/events/:id', eventsApi.del);

    app.get('/api/pfio', pfioApi.fire)
};

