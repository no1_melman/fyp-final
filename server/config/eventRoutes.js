/**
 * Created by Callum on 04/04/14.
 */

var EventBus = require('../modules/EventBus'), // Event bus
    pfioApi = require('../api/pfio')(); // PiFace API functions

// PiFace events routing
EventBus.on('pfio.input.changed', pfioApi.fireEvent);

// Events routing
EventBus.on('event.fire', function (event) {
    pfioApi.modifyPfioState(event.eventEntity, true, event.id); // because we want the event to passed to the pfio to modify
    // its state, fire the function that deals with that

    // this is modular because the events aren't directly tied in with the pfio
    // if you removed this module and replaced it with some other output device then
    // you would just put this EventBus in that module and pick up the event
});