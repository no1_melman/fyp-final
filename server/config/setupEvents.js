/**
 * Created by Callum on 23/03/14.
 */
var colors = require('colors'),
    util = require('util'),
    async = require('async'),
    events = require('./dbModels').event,
    devices = require('./dbModels').device,
    Event = require('./dbModels').event;

var setupEvent = function (done) {
    devices.find({}, function (err, foundDevices) {
        if (!foundDevices){
            done(null, "No devices found");
            return;
        }

        if (!(foundDevices instanceof Array)) {
            done(null, "There weren't any devices");
            return;
        }

        if (foundDevices.length < 1) {
            done(null, "Empty array of devices");
            return;
        }

        var sortedDevices = separateDevices(foundDevices);

        // New Date of todays time plus 20 seconds
        var d = new Date();
        d.setSeconds(d.getSeconds() + 20);

        events.find({}, function (err, eventsFound){
            if (err) {
                console.log(colors.red(util.inspect(err)));
                return;
            }

            async.each(eventsFound, function(event, callback){
                event.remove(function (err){
                    if (err){
                        callback(err);
                        return;
                    }
                    callback();
                });
            },function (err){
                if (err){
                    console.log(colors.red(util.inspect(err)));
                    return;
                }

                var event = {
                    eventType: 3,
                    deviceOutputs: [ sortedDevices.output[0].id, sortedDevices.output[2].id ],
                    when: d.getTime(),
                    repeat: false
                };
                var event1 = {
                    eventType: 1,
                    deviceInput:sortedDevices.input[1].id,
                    deviceOutputs: [ sortedDevices.output[3].id, sortedDevices.output[4].id ],
                    when: d.getTime(),
                    repeat: false
                };
                var event2 = {
                    eventType: 1,
                    deviceInput:sortedDevices.input[2].id,
                    deviceOutputs: [ sortedDevices.output[5].id, sortedDevices.output[6].id ],
                    when: d.getTime(),
                    repeat: false
                };

                events.create([event, event1, event2],function (err){
                    if (err) {
                        done(err, "Error creating event");
                        return;
                    }

                    console.log("Created %s %s", colors.yellow("3"),colors.green("events"));

                    done(null, "Successfully created event");
                });
            });
        });
    });
};

module.exports = setupEvent;

function separateDevices(devices) {
    var sortedDevices = {
        input: new Array(),
        output: new Array()
    }

    for (var i = 0; i < devices.length; i++){
        var device = devices[i];
        if (device.input)
            sortedDevices.input.push(device);
        else
            sortedDevices.output.push(device);
    }

    return sortedDevices;
}