var EventBus = require('../EventBus'),
    pfio = require('piface-node'),
    colors = require('colors');

require('./pfio.input.changed');
require('./pfio.input.hold');

var pfioServer = function () {
    this.prev_state = 0;
    this.start = function () {
        pfio.init();
        this.watchInputs();
	    EventBus.emit('pfio.server.start');
    };
    this.watchInputs = function () {
        var state = pfio.read_input();

        if (state !== this.prev_state) {
            EventBus.emit('pfio.inputs.changed', state, this.prev_state);
            this.prev_state = state;
        }

        setTimeout(this.watchInputs.bind(this), 10); // going to put this on the event loop next event, more efficient
    };
    this.stop = function (state) {
        pfio.write_output(state);
        pfio.deinit();
        console.log("%s", colors.green("PiFace Server Shutdown"));
    };
};

EventBus.on('pfio.change.output', function (state) {
    pfio.write_output(state);
});

// export the object as a module
module.exports = new pfioServer();
