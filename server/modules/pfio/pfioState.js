/**
 * Created by Callum on 14/02/14.
 */
var Binary = require('../../helpers/binaryHelper');


exports.allOff = function () {
    return new Binary(0);
};
exports.allOn = function () {
    return new Binary(255);
};
exports.current = new Binary(0);
