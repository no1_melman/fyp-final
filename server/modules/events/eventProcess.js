/**
 * Created by Callum on 03/04/14.
 */
var colors = require('colors'),
    eventQueue = require('./eventQueue'),
    eventBus = require('../EventBus');

eventBus.on('event.process', processEvent);

function processEvent(index) {
    var event = getEvent(index);

    var current = new Date().getTime(); // get the number of milliseconds from 1/1/1970

    var fireEvent = false;

    if (event.eventType == 2) // timer or interval respectively
        fireEvent = current - event.init.getTime() >= event.when;

    if (event.eventType == 3) // schedule
        fireEvent = current >= event.when;

    if (fireEvent)
        eventBus.emit('event.fire', event);
    else
        event.processing = false;
}

function getEvent(index){
    return eventQueue[index];
}