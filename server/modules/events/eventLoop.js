var eventBus = require('../EventBus'),
    eventQueue = require('./eventQueue'),
    guid = require('../../helpers/guidHelper'),
    events = require('../../config/dbModels').event,
    devices = require('../../config/dbModels').device,
    util = require('util'),
    colors = require('colors'),
    async = require('async');

function executeEvent() {
    if (eventQueue.length > 0) { // check that there are events in the queue
        for (var i = 0; i < eventQueue.length; i++) { // iterate through all items
            var event = eventQueue[i]; // get the event object

            if (event.processing == false) { // see if it is processing it
                event.processing = true; // if it isn't say it is now
                eventBus.emit('event.process', i); // emit the event to all listeners
            }
        }
    }

    setImmediate(executeEvent.bind(this)); // set this to immediately execute in the next process tick
};

// do something with an event
exports.executeEvent = executeEvent;

// create the eventQueue
exports.readEvents = function (done) {
    // eventTypes: immediate, reading, timer, scheduled
    // the interval eventType is defined by the repeat boolean.

    events.find({eventType: { $gte: 1 }}, function (err, events) {
        if (err) { // find all the events, check for errors
            console.log("%s", colors.red(util.inspect(err)));
            done(err); // if so pass that error down the chain
            return; // stop processing this function
        }

        if (events.length > 0) { // check to make sure there are some events
            devices.find({}, function (err, foundDevices) { // find all the devices
                if (err) { // if there has been any errors
                    console.log("%s", colors.red(util.inspect(err)));
                    done(err);
                    return;
                }

                async.each(events, function (event, callback){ // for each event
                    var matchedDevices = []; // get a set of matched devices

                    async.each(foundDevices, function (device, callback) {
                            if (any(event.deviceOutputs, device.id)) // check if any of the devices equals the id of this device
                                matchedDevices.push(device); // if so then added it to the list

                            callback(); // just say we're down move onto the next one
                        },
                        function (err) {
                            if (err){
                                console.log(colors.red(util.inspect(err))); // if there has been an error inspect it and display
                                callback();
                                return; // get out of the function
                            }

                            if (matchedDevices.length > 0) { // check to make sure there are matches
                                var internalEventObject = {
                                    id: guid.createGuid(), // Events need a unique id to test against in order to later say which event has finished.
                                    // use the guid creator from ups files
                                    when: event.when, // when the event will happen
                                    init:new Date(), // used in the event loop to test when the "when" is it
                                    repeat: event.repeat,
                                    eventType: event.eventType,
                                    affectedDevices: matchedDevices, // add the affected devices to this
                                    eventEntity: event,
                                    processing: false
                                };
                                eventQueue.push(internalEventObject); // push the event to the internal event object
                                console.log("%s to event queue", colors.green("Event added"));
                            }

                            callback();
                        });
                }, function (err){
                    if (err){
                        done(err);
                        return;
                    }
                    done();
                });
            });

            return; // the done should be executed in the event queue creation so just need to return here
        }

        console.log(colors.magenta("There are no events"));
        done(); // obviously we need this to make sure that if there are no events, the done still gets fired off
    });
};

function any(arrayOfItems, id) {
    var success = false; // this is to signal that we have found a match

    async.each(arrayOfItems, function (item, callback) { // iterate over all the array
        if (id === item.toHexString()) // if the id is equal to one of the items
            success = true; // success is true

        callback(); // say that we have finished
    }, function (err) {

    });

    return success; // we don't care about errors, just return the success status
}
