/**
 * Created by Callum on 14/02/14.
 */

module.exports = function(input) {
    var self = this;
    self.binaryArray = [0,0,0,0,0,0,0,0];

    if (typeof input === 'string') {
        if (input.length != 8)
            throw new Error("Parameter string is not the correct length");

        var pattern = new RegExp("[2-9]", "g");
        var res = pattern.test(input);
        if (res)
            throw new Error("Input string not in the correct binary format");

        for (var i = 0; i < 8; i++){
            self.binaryArray[i] = input[i];
        }
    } else if (input instanceof Array) {
        if (input.length != 8)
            throw new Error("Parameter array is not the correct length");

        for (var i = 0; i < 8; i++){
            if (typeof input[i] !== 'number')
                throw new Error("Array values aren't a number");

            if (input[i] > 1)
                    throw new Error("Input array not in the correct binary format");
        }

        self.binaryArray = input;
    } else if (typeof input ==='number'){
        if (input > 255 || input < 0)
            throw new Error("Parameter isn't within the correct bounds for signed integer");

        var binaryCounter = 7;
        while(input > 0) {
            var d = input % 2;

            if (d > 0) {
                self.binaryArray[binaryCounter] = 1;
            }

            input = Math.floor(input / 2);
            binaryCounter--;
        }
    } else if (typeof input === 'object') {
        throw new Error("Unsupported input type")
    } else {
        throw new Error("Can't parse parameter");
    }

    return {
        toString: function () {
            var stringy = "";
            for (var i = 0; i < 8; i++){
                stringy += self.binaryArray[i];
            }
            return stringy;
        },
        toArray: function () { return self.binaryArray; },
        toObject: function () { return {
            1: self.binaryArray[0],
            2: self.binaryArray[1],
            3: self.binaryArray[2],
            4: self.binaryArray[3],
            5: self.binaryArray[4],
            6: self.binaryArray[5],
            7: self.binaryArray[6],
            8: self.binaryArray[7]
        };},
        toDecimal: function () {
            return parseInt(self.binaryArray.join(''), 2);
        }
    };
}